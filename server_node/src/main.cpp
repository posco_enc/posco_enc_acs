
#include <QtGui>
#include <QApplication>

#include "agv_protocol/tcpManager.h"

int main(int argc, char **argv) {

    QCoreApplication app(argc, argv);

    tcpManager w(argc,argv);// = new tcpManager(argc,argv, nullptr);
    QObject::connect(&w, SIGNAL(finished()), &app, SLOT(quit()));
        
    int result = app.exec();

	return result;
}
