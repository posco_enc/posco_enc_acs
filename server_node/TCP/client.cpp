#include "client.h"
#include <cmath>
#include <QDataStream>
//static inline QByteArray IntToArray(qint32 source);
Client::Client(QString ip_addr, quint16 port, QObject * parent)
    : QObject (parent),
      ip_addr(ip_addr),
      port (port)
{
    qDebug()<<"TCP client Start :"<<ip_addr<<port;
}

void Client::setIP(QString str_ip)
{
      ip_addr = str_ip;
}

void Client::setPort(quint16 uint16_port)
{
      port = uint16_port;
}

void Client::doConnect(QString str_ip, quint16 uint16_port)
{
	setIP(str_ip);
	setPort(uint16_port);

	doConnect();
}

void Client::doSend(QString str)
{
	QByteArray str_;
	str_.append(str);

	writeData(str_);
}

bool Client::doConnect()
{
    socket = new QTcpSocket(this);
    connect(socket, SIGNAL(readyRead()), this, SLOT(readReceivedData()));
    socket->connectToHost(ip_addr, port);
   if(!socket->waitForConnected(100))
    {
        qDebug()<<"client connection"<<false;
         return false;
    }
    else
    {
        qDebug()<<"client connection"<<true;
        return true;
    }
   // return socket->waitForConnected(500);
}
bool Client::writeData(QByteArray data)
{
    if(socket->state() == QAbstractSocket::ConnectedState)
    {
        socket->write(data); //write the data itself;
        qDebug()<<"write"<<data;
        return socket->waitForBytesWritten(-1);
    }
    else
    {
        qDebug()<<socket->state();
        return false;
    }
}
void Client::doDisConnect()
{
    socket->close();
    socket->deleteLater();
}


void Client::readReceivedData()
{
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    QByteArray buffer;
    qDebug()<< "Client Receive";
    while(socket->bytesAvailable())
    {
        buffer.append(socket->readLine());
//        qDebug()<<socket->readAll();
    }
    QString str= QString(buffer);
    qDebug()<<buffer;
	Q_EMIT readData(buffer);
	Q_EMIT readData(str);
//    qDebug()<< str.length()<<str;
 }
