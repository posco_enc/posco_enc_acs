#include "m_udp.h"

m_Udp::m_Udp(QObject *parent)
    : QObject(parent)
{
    socket = new QUdpSocket(this);
    //if(socket->bind(QHostAddress("192.168.2.3"), 1234))
    if(socket->bind(QHostAddress("192.168.40.3"), 1234))
    {
        qDebug()<<"bind succes";
        connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    }
    else
        qDebug()<<"bind fail";
}

void m_Udp::HelloUDP()
{
    qDebug()<<"hello";
    QByteArray Data;
    Data.append("Hello World");

    //socket->writeDatagram(Data, QHostAddress("192.168.2.2"), 1234);
    socket->writeDatagram(Data, QHostAddress("192.168.40.3"), 1234);
}

void m_Udp::readyRead()
{
    qDebug()<<"read";
    QByteArray buffer;
    buffer.resize(static_cast<qint32>(socket->pendingDatagramSize()));

    QHostAddress sender;
    quint16 senderPort;

    /*socket->readDatagram(buffer.data(), buffer.size(),
                         &sender;, &senderPort;);
                         */
    socket->readDatagram(buffer.data(), buffer.size(), &sender, &senderPort);

    qDebug() << "Message from: " << sender.toString();
    qDebug() << "Message port: " << senderPort;
    qDebug() << "Message: " << buffer;
}

void m_Udp::spin()
{
}
