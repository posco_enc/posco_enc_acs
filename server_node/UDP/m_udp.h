#ifndef M_UDP_H
#define M_UDP_H

#include <QObject>
#include <QUdpSocket>
//#include <ros/ros.h>

class m_Udp : public QObject
{
    Q_OBJECT
public:
    explicit m_Udp(QObject *parent = nullptr);
    void HelloUDP();
    void spin();
signals:

public slots:
    void readyRead();

private:
    QUdpSocket *socket;
};

#endif // M_UDP_H
