

#ifndef protocol_sample_QNODE_HPP_
#define protocol_sample_QNODE_HPP_

#ifndef Q_MOC_RUN

#include <string>
#include <QThread>
#include <QStringListModel>

#endif
#include <ros/ros.h>
#include <ros/network.h>

//#include <sis_msgs/cmd_TASK.h>
#include <sis_msgs/state_Server.h>
//#include <sis_msgs/state.h>
#include <sis_msgs/cmd_AGV.h>
#include <sis_msgs/state_AGV.h>
#include <sis_msgs/initPos.h>


struct STATE
{
    int8_t id = 0;
    int16_t X = 0;
    int16_t Y = 0;
    int32_t A = 0;

    uint8_t agv_pos = 0;

    uint8_t agv_status = 0;
    uint8_t conv_status = 0;
    uint8_t emc = 0;

    int16_t main_vol = 0;

    int8_t conv_in = 0;
    int8_t conv_mid = 0;
    int8_t conv_arr = 0;

    int8_t stp1_up = 0;
    int8_t stp1_down = 0;
    int8_t stp2_up = 0;
    int8_t stp2_down = 0;
};


class QNode : public QThread {
    Q_OBJECT
    
public:
	QNode(int argc, char** argv );
	virtual ~QNode();
	
	bool init();
	void run();

    void cmdCallback(const sis_msgs::cmd_AGV::ConstPtr& msg);
    
	enum LogLevel {
	         Debug,
	         Info,
	         Warn,
	         Error,
	         Fatal
	 };

	QStringListModel* loggingModel() { return &logging_model; }
	void log( const LogLevel &level, const std::string &msg);

Q_SIGNALS:
	void loggingUpdated();
    void rosShutdown();
    void send_CMD(quint8 agv_id, quint8 task_cmd, qint8 goal, qint8 sub_goal, qint16 dist_x, qint16 dist_y, qint16 dist_th, qint8 conv);

public Q_SLOTS:
	void client_disconnect(int, int);
	void client_connect(int, int);
	
	void send_State(STATE);
	
private:
	int init_argc;
	char** init_argv;

    QStringListModel logging_model;
    
	ros::Publisher pub_port;
	
	ros::Publisher pub_state;
    ros::Subscriber sub_CMD, sub_init;

    sis_msgs::state_Server port_msgs;
    int8_t cnt1_ = 0, cnt2_ = 0;
    bool agv1start_ = false, agv2start_ = false;
};

#endif /* protocol_sample_QNODE_HPP_ */
