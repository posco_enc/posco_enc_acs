#include <QDebug>
#include <agv_protocol/tcpManager.h>

#define CMD_PORT 30002
#define STATE_PORT 30003

tcpManager::tcpManager(int argc, char** argv, QObject* parent)
    : QObject(parent), cmd_server(new Server(CMD_PORT, this)),
      state_server(new Server(STATE_PORT, this)),
      server_node(new QNode(argc, argv)), state_Timer(new QTimer(this))
{
  //    setObjectName(QString("Task Manager"));
  //    QObject::connect(cmd_server, SIGNAL(add()), this, SLOT(add()));
  QObject::connect(server_node, SIGNAL(rosShutdown()), this, SLOT(close()));
  QObject::connect(server_node, SIGNAL(rosShutdown()), state_server,
                   SLOT(close_()));
  QObject::connect(server_node, SIGNAL(rosShutdown()), cmd_server,
                   SLOT(close_()));

  QObject::connect(
      server_node,
      SIGNAL(
          send_CMD(quint8, quint8, qint8, qint8, qreal, qreal, qreal, qint8)),
      this,
      SLOT(send_CMD(quint8, quint8, qint8, qint8, qreal, qreal, qreal, qint8)));
  // check agv_state
  QObject::connect(state_server, SIGNAL(readInfo(int, QByteArray)), this,
                   SLOT(readInfo(int, QByteArray)));
  QObject::connect(state_Timer, SIGNAL(timeout()), this, SLOT(send_State()));
  // check port_connet
  QObject::connect(cmd_server, SIGNAL(client_connect(int, int)), server_node,
                   SLOT(client_connect(int, int)));
  QObject::connect(cmd_server, SIGNAL(client_disconnect(int, int)), server_node,
                   SLOT(client_disconnect(int, int)));
  // check port_connet
  QObject::connect(state_server, SIGNAL(client_connect(int, int)), server_node,
                   SLOT(client_connect(int, int)));
  QObject::connect(state_server, SIGNAL(client_disconnect(int, int)),
                   server_node, SLOT(client_disconnect(int, int)));
}

tcpManager::~tcpManager() { deleteLater(); }

void tcpManager::close() { emit finished(); }

void tcpManager::setIP(std::string ip_) {}

void tcpManager::setPort(quint16 port_) {}

void tcpManager::readData(QByteArray data)
{
  int size;
  QByteArray ID, CMD, DATA;

  size = data.size();
  ID = data.mid(0, 1);
  CMD = data.mid(1, 1);

  if ((CMD.toHex() == "01") && (size > 2))
    QByteArray DATA = data.mid(2, size - 2);
}

void tcpManager::add()
{
  //    state_msgs.data = false;
}


void tcpManager::send_CMD(quint8 agv_id, quint8 task_cmd, qint8 goal,
                          qint8 sub_goal, qint16 dist_x, qint16 dist_y,
                          qint32 dist_th, qint8 conv)
{
  QByteArray STX, ETX, LEN;
  STX.resize(1);
  LEN.resize(2);
  ETX.resize(1);

  STX[0] = 0x01;
  LEN = shortTo2Byte(17);
  ETX[0] = 0x01;

  QByteArray Id_;
  Id_.resize(1);
  Id_[0] = agv_id;

  QByteArray Cmd_;
  Cmd_.resize(1);
  Cmd_[0] = task_cmd;

  QByteArray Conv_;
  Conv_.resize(1);
  Conv_[0] = conv;

  QByteArray Goal_;
  Goal_.resize(1);
  Goal_[0] = goal;

  QByteArray SubGoal_;
  SubGoal_.resize(1);
  SubGoal_[0] = sub_goal;

  QByteArray X_;
  X_.resize(2);
  X_ = shortTo2Byte(dist_x);

  QByteArray Y_;
  Y_.resize(2);
  Y_ = shortTo2Byte(dist_y);

  QByteArray A_;
  A_.resize(4);
  A_ = IntTo4Byte(dist_th);

  QByteArray Data;
  Data.append(STX);      // 1 byte
  Data.append(LEN);      // 2 byte
  Data.append(Id_);      // 1 byte
  Data.append(Cmd_);     // 1 byte
  Data.append(Goal_);    // 1 byte
  Data.append(SubGoal_); // 1 byte
  Data.append(X_);       // 2 byte
  Data.append(Y_);       // 2 byte
  Data.append(A_);       // 4 byte
  Data.append(Conv_);    // 1 byte
  Data.append(ETX);      // 1 byte

  cmd_server->writeData(agv_id, Data);
}

void tcpManager::send_State()
{
  server_node->send_State(state_1);
  server_node->send_State(state_2);
}

void tcpManager::readInfo(int id, QByteArray data)
{
  if (id == 1)
  {
    state_1.id = 1;
    state_1.X = QByteToShort(data.mid(0, 2));
    state_1.Y = QByteToShort(data.mid(2, 2));
    state_1.A = QByteToInt(data.mid(4, 4));

    state_1.main_vol = QByteToShort(data.mid(8, 2));

    state_1.agv_pos = QByteToShort(data.mid(10, 1));

    state_1.agv_status = QByteToShort(data.mid(11, 1));
    state_1.conv_status = QByteToShort(data.mid(12, 1));
    state_1.emc = QByteToShort(data.mid(13, 1));
    state_1.main_vol = QByteToShort(data.mid(14, 2));

    state_1.conv_in = QByteToShort(data.mid(16, 1));
    state_1.conv_mid = QByteToShort(data.mid(17, 1));
    state_1.conv_arr = QByteToShort(data.mid(18, 1));

    state_1.stp1_up = QByteToShort(data.mid(19, 1));
    state_1.stp1_down = QByteToShort(data.mid(20, 1));
    state_1.stp2_up = QByteToShort(data.mid(21, 1));
    state_1.stp2_down = QByteToShort(data.mid(22, 1));

    server_node->send_State(state_1);
  }
  else if (id == 2)
  {
    state_2.id = 2;
    state_2.X = QByteToShort(data.mid(0, 2));
    state_2.Y = QByteToShort(data.mid(2, 2));
    state_2.A = QByteToInt(data.mid(4, 4));

    state_2.main_vol = QByteToShort(data.mid(8, 2));

    state_2.agv_pos = QByteToShort(data.mid(10, 1));

    state_2.agv_status = QByteToShort(data.mid(11, 1));
    state_2.conv_status = QByteToShort(data.mid(12, 1));
    state_2.emc = QByteToShort(data.mid(13, 1));
    state_2.main_vol = QByteToShort(data.mid(14, 2));

    state_2.conv_in = QByteToShort(data.mid(16, 1));
    state_2.conv_mid = QByteToShort(data.mid(17, 1));
    state_2.conv_arr = QByteToShort(data.mid(18, 1));

    state_2.stp1_up = QByteToShort(data.mid(19, 1));
    state_2.stp1_down = QByteToShort(data.mid(20, 1));
    state_2.stp2_up = QByteToShort(data.mid(21, 1));
    state_2.stp2_down = QByteToShort(data.mid(22, 1));

    server_node->send_State(state_2);
  }
}
