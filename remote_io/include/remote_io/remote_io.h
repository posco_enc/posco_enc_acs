#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <modbus.h>
#include "std_msgs/UInt16.h"
#include "std_msgs/UInt8.h"
#include <ros/ros.h>

#include "io_config.h"

const int EXCEPTION_RC = 2;

enum {
    TCP,
    TCP_PI,
    RTU
};



class remoteIO
{

public:
  remoteIO();
  int run();

protected:
  ros::NodeHandle nh;

  ros::Publisher pub_io_data, pub_io_port;

  // host/port settings
  std::string host="192.168.2.211";
  int port=502;

  modbus_t *ctx = NULL;
  uint8_t *tab_rp_bits = NULL;
  //uint32_t old_response_to_sec;
  //uint32_t old_response_to_usec;
  int rc;
  void publish_io_data(uint16_t data);

};



