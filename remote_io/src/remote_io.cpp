#include <remote_io.h>

remoteIO::remoteIO()
{

  // CBA Read local params (from launch file)
  ros::NodeHandle nhLocal("~");

  nhLocal.param<std::string>("host", host, "192.168.2.211");
  nhLocal.param("port", port, 502);

  pub_io_data = nh.advertise<std_msgs::UInt16>("/IO/DATA", 1, this);
  pub_io_port = nh.advertise<std_msgs::UInt8>("/IO/PORT", 1, this);
}

void remoteIO::publish_io_data(uint16_t data)
{
  std_msgs::UInt16 data_msgs;
  data_msgs.data = data;
  pub_io_data.publish(data_msgs);
}

int remoteIO::run()
{
  char* host_ip = const_cast<char*>(host.c_str());
  ctx = modbus_new_tcp(host_ip, port);
  if (ctx == NULL)
  {
    printf("Unable to allocate libmodbus context\n");
    return -1;
  }

  // modbus_get_response_timeout(ctx, &old_response_to_sec,
  // &old_response_to_usec);

  if (modbus_connect(ctx) == -1)
  {
    printf("Connection failed: \n");
    modbus_free(ctx);
    return -1;
  }

  /* Allocate and initialize the memory to store the bits */

  tab_rp_bits = (uint8_t*)malloc(UT_INPUT_BITS_NB * sizeof(uint8_t));
  memset(tab_rp_bits, 0, UT_INPUT_BITS_NB * sizeof(uint8_t));

  ros::Rate loopRate(20);

  while (ros::ok())
  {
    rc = modbus_read_input_bits(ctx, UT_INPUT_BITS_ADDRESS, UT_INPUT_BITS_NB,
                                tab_rp_bits);
    uint16_t _read_data = 0;
    for (int i = 0; i++; i < UT_INPUT_BITS_NB)
    {
      _read_data += tab_rp_bits[i] << i;
    }
    publish_io_data(_read_data);

    loopRate.sleep();
  }

  free(tab_rp_bits);

  modbus_close(ctx);
  modbus_free(ctx);

  return 0;
}

int main(int argc, char** argv)
{

  ros::init(argc, argv, "remote io");

  remoteIO node;

  return node.run();
}
