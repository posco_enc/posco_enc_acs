
#include <ros/ros.h>
#include <std_msgs/Int8.h>
#include <QProcess>

void shutdown_Callback(const std_msgs::Int8::ConstPtr& msg)
{
    if(msg->data==1)
    {
        QProcess process;
        QStringList list;
        list << "/home/ipc/sh/exit.sh";
        process.execute("/bin/bash",list);
        
        process.startDetached("shutdown -h now");
    }
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "sis_shutdown_node");
    
    ros::NodeHandle nh;
    ros::Subscriber sub = nh.subscribe("/Display/ACS/Shutdown", 1, &shutdown_Callback);

    ros::spin();
    
    return 0;
};

