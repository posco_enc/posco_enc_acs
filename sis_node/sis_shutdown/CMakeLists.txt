cmake_minimum_required(VERSION 2.8.3)
project(sis_shutdown)

add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
)

find_package(Qt5Widgets REQUIRED)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

include_directories(${CMAKE_CURRENT_BINARY_DIR})
include_directories(${Qt5Widgets_INCLUDES})

catkin_package(
  CATKIN_DEPENDS roscpp std_msgs
  DEPENDS system_lib

)

include_directories(
# include
  ${catkin_INCLUDE_DIRS}
)

add_executable(${PROJECT_NAME}_node src/main.cpp)
target_link_libraries(${PROJECT_NAME}_node   ${catkin_LIBRARIES}
                                             ${Qt5Widgets_LIBRARIES}
                     )
                 ##################
                                  ### install
                                  ###################
                                  ## Mark executables and/or libraries for installation
                                  #install(TARGETS
                                  #  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
                                  #  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
                                  #  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
                                  #)
                                  install(TARGETS
                                     sis_shutdown_node
                                     ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
                                     LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
                                     RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
                                   )


                                  ## Mark cpp header files for installation
                                  install(DIRECTORY include/${PROJECT_NAME}/
                                     DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
                                     FILES_MATCHING PATTERN "*.h"
                                     PATTERN ".svn" EXCLUDE
                                  )

                                  #install(DIRECTORY srv/
                                  #  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/srv
                                  #  FILES_MATCHING PATTERN "*.srv"
                                  #)

                                  #install(DIRECTORY msg/
                                  #  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/msg
                                  #  FILES_MATCHING PATTERN "*.msg"
                                  #)

                                  #install(
                                  #  DIRECTORY launch
                                  #  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
                                  #)
