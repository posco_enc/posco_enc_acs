#ifndef MYVIZ_H
#define MYVIZ_H

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QWidget>

namespace Ui
{
class myViz;
}

class myViz : public QWidget
{
  Q_OBJECT

public:
  explicit myViz(QWidget* parent = 0);
  ~myViz();

public slots:
  void agvPos(qreal, qreal, qreal);

private:
  double ratio_ = 3779.5275591;
  Ui::myViz* ui;
  QGraphicsScene* scene;

  QGraphicsPixmapItem *item_AGV1;
};

#endif // MYVIZ_H
