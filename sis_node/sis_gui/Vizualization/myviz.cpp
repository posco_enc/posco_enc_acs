#include "myviz.h"
#include "ui_myviz.h"

myViz::myViz(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::myViz)
{
    ui->setupUi(this);

    QPixmap pix_AGV1 = QPixmap(":/resource/posco_agv.png");

    item_AGV1 = new QGraphicsPixmapItem(pix_AGV1);

    ui->graphicsView->scale(0.6, 0.6);
    ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    QRect rcontent = ui->graphicsView->contentsRect();
    ui->graphicsView->setSceneRect(0, 0, rcontent.width(), rcontent.height());
    scene = new QGraphicsScene(ui->graphicsView->contentsRect());

    scene->addItem(item_AGV1);

    item_AGV1->setOffset(-45,-45);


    double x = 0.0, y = 0.0;

    double Xc = -460+x*70.5, Yc = -80  - y*65;

    item_AGV1->setPos(Xc,Yc);

    item_AGV1->setRotation(0);



}

myViz::~myViz()
{
    delete ui;
}

void myViz::agvPos(qreal x, qreal y, qreal theta)
{
    double Xc = -460+x*78, Yc = -90  - y*90;
    item_AGV1->setPos(Xc, Yc);
    item_AGV1->setRotation(-theta);

}

