#ifndef SET_FRAME_H
#define SET_FRAME_H

#include <QWidget>
#include <QPushButton>
#include <QSettings>
#include <QObject>
#include <QDebug>


namespace Ui {
class set_frame;
}

class Form;

class set_frame : public QWidget
{
    Q_OBJECT

public:
    explicit set_frame(QWidget *parent = 0);
    ~set_frame();

    void init_posTable();
    void init_taskTable();

    void set_posTable(int index);
    void set_taskTable(int index);

    void writePosSettings(int iSndex);
    void writeTaskSettings(int index);

    QPushButton *btn_editPos[9];
    QPushButton *btn_editTask[12];
    QStringList pos_code, task_code;

    int pos_index = 0, task_index = 0;


private slots:

private:
    Ui::set_frame *ui;

    QFont font, font_h;

};

#endif // SET_FRAME_H
