#include "set_frame.h"
#include "ui_set_frame.h"
#include <QProcess>

set_frame::set_frame(QWidget* parent) : QWidget(parent), ui(new Ui::set_frame)
{
  ui->setupUi(this); // setup ui
  init_posTable();
  init_taskTable();
  /*
  pos_code << tr("자재") << tr("충전") << tr("시험")
           << tr("환경01-L") << tr("환경01-R")
           << tr("환경02-L") << tr("환경02-R")
           << tr("환경03-L") << tr("환경03-R");

  task_code << tr("TQ01-L-L") << tr("TQ01-L-H") << tr("TQ01-R-L") << tr("TQ01-R-H")
            << tr("TQ02-L-L") << tr("TQ02-L-H") << tr("TQ02-R-L") << tr("TQ02-R-H")
            << tr("TQ03-L-L") << tr("TQ03-L-H") << tr("TQ03-R-L") << tr("TQ03-R-H");
            */
/*
  pos_code << tr("SHOP") << tr("CHG") << tr("TEST")
           << tr("RA1L") << tr("RA1R")
           << tr("RA2L") << tr("RA2R")
           << tr("RA3L") << tr("RA3R");

  task_code << tr("R1LL") << tr("R1LH") << tr("R1RL") << tr("R1RH")
            << tr("R2LL") << tr("R2LH") << tr("R2RL") << tr("R2RH")
            << tr("R3LL") << tr("R3LH") << tr("R3RL") << tr("R3RH");
            */


  pos_code << tr("자재") << tr("충전") << tr("시험")
           << tr("TQ01-L") << tr("TQ01-R")
           << tr("TQ02-L") << tr("TQ02-R")
           << tr("TQ03-L") << tr("TQ03-R");   // left/right

  task_code << tr("TQ01-D") << tr("TQ01-U")   // down/up
            << tr("TQ02-D") << tr("TQ02-U")
            << tr("TQ03-D") << tr("TQ03-U");



}

set_frame::~set_frame() { delete ui; }

void set_frame::init_posTable()
{

  // For position table
  ui->table_pos->clear();

  ui->table_pos->setRowCount(9);
  ui->table_pos->setColumnCount(7);

  ui->table_pos->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  ui->table_pos->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  ui->table_pos->verticalHeader()->hide();
  ui->table_pos->setEditTriggers(QAbstractItemView::DoubleClicked);

  font_h.setBold(true);
  font_h.setPointSize(10);

  ui->table_pos->horizontalHeader()->setFont(font_h);
  ui->table_pos->verticalHeader()->setFont(font_h);
  // ui->table_pos->verticalHeader()->setFixedHeight(330);

  QStringList pos_Header = QStringList() << tr("POS") << tr("ID") << tr("X")
                                         << tr("Y") << tr("A") << tr("QR") << tr("SAVE");
  ui->table_pos->setHorizontalHeaderLabels(pos_Header);
  ui->table_pos->setSelectionMode(QAbstractItemView::NoSelection);
  ui->table_pos->setFocusPolicy(Qt::NoFocus);
  ui->table_pos->resizeRowsToContents();
}
void set_frame::init_taskTable()
{
  // For task table
  ui->table_task->clear();

  ui->table_task->setRowCount(6);
  ui->table_task->setColumnCount(5);

  ui->table_task->horizontalHeader()->setSectionResizeMode(
      QHeaderView::Stretch);
  ui->table_task->setEditTriggers(QAbstractItemView::DoubleClicked);
  ui->table_task->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);

  ui->table_task->horizontalHeader()->setFont(font_h);
  ui->table_task->verticalHeader()->setFont(font_h);
  ui->table_task->verticalHeader()->hide();
  // ui->table_task->verticalHeader()->setFixedHeight(250);

  QStringList task_Header = QStringList() << tr("CHAMBER") << tr("Z") << tr("TABLE")
                                          << tr("PUSHER") << tr("SAVE");
  ui->table_task->setHorizontalHeaderLabels(task_Header);
  ui->table_task->setSelectionMode(QAbstractItemView::NoSelection);
  ui->table_task->setFocusPolicy(Qt::NoFocus);
  ui->table_task->resizeRowsToContents();
}

void set_frame::set_posTable(int index)
{
  init_posTable();

  pos_index = index;

  // Set name for table
  QSettings* setPos;
  setPos = new QSettings("ACS", "POS");

  QStringList tmp;
  tmp << tr("0") << tr("0.0") << tr("0.0") << tr("0.0") << tr("0");

  font.setBold(false);
  font.setPointSize(12);

  for (int i = 0; i < index; i++)
  {

    QTableWidgetItem* item_Name = new QTableWidgetItem(pos_code[i]);
    item_Name->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);
    item_Name->setForeground(Qt::black);
    item_Name->setFont(font);

    QTableWidgetItem* item_ID = new QTableWidgetItem(
        setPos->value(pos_code[i], tmp).value<QStringList>()[0]);
    item_ID->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);
    item_ID->setForeground(Qt::black);
    item_ID->setFont(font);

    QTableWidgetItem* item_X = new QTableWidgetItem(
        setPos->value(pos_code[i], tmp).value<QStringList>()[1]);
    item_X->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);
    item_X->setForeground(Qt::black);
    item_X->setFont(font);

    QTableWidgetItem* item_Y = new QTableWidgetItem(
        setPos->value(pos_code[i], tmp).value<QStringList>()[2]);
    item_Y->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);
    item_Y->setForeground(Qt::black);
    item_Y->setFont(font);

    QTableWidgetItem* item_A = new QTableWidgetItem(
        setPos->value(pos_code[i], tmp).value<QStringList>()[3]);
    item_A->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);
    item_A->setForeground(Qt::black);
    item_A->setFont(font);

    QTableWidgetItem* item_Q = new QTableWidgetItem(
        setPos->value(pos_code[i], tmp).value<QStringList>()[4]);
    item_A->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);
    item_A->setForeground(Qt::black);
    item_A->setFont(font);

    ui->table_pos->setItem(i, 0, item_Name);
    ui->table_pos->setItem(i, 1, item_ID);
    ui->table_pos->setItem(i, 2, item_X);
    ui->table_pos->setItem(i, 3, item_Y);
    ui->table_pos->setItem(i, 4, item_A);
    ui->table_pos->setItem(i, 5, item_Q);

    // Button save value

    btn_editPos[i] = new QPushButton;
    ui->table_pos->setCellWidget(i, 6, btn_editPos[i]);

    btn_editPos[i]->setText("SAVE");
    btn_editPos[i]->setStyleSheet("QPushButton"
                                  "{"
                                  "    background-color: rgb(225, 225, 201);"
                                  "    background-color: #1f425d;"
                                  "    background-color: #3cbaa2;"
                                  "    border: 4px outset #a3a1a1;"
                                  "    font: 75 12pt;"
                                  "    font-weight: 550;"
                                  "    color:rgb(240, 240, 240);"
                                  "}"

                                  "QPushButton:pressed"
                                  "{"
                                  "    border: 4px inset #a3a1a1;"
                                  "    background-color: cyan;"
                                  "    background-color: #3cbaa2;"
                                  "    background-color: rgb(239, 239, 239);"
                                  "    color:rgb(40, 40, 40);"
                                  "    font-weight: 400;"
                                  "}");

    QObject::connect(btn_editPos[i], &QPushButton::clicked,
                     [this, i]() { writePosSettings(i); });
  }
}

void set_frame::writePosSettings(int index)
{
  QString key_name = pos_code[index];

  QStringList value_;
  value_ << ui->table_pos->item(index, 1)->text()  //id
         << ui->table_pos->item(index, 2)->text()  //x
         << ui->table_pos->item(index, 3)->text()  //y
         << ui->table_pos->item(index, 4)->text()  //a
         << ui->table_pos->item(index, 5)->text(); //qr

  QSettings* setPos;
  setPos = new QSettings("ACS", "POS");

  setPos->setValue(key_name, value_);
  set_posTable(pos_index);
}

void set_frame::set_taskTable(int index)
{
  init_taskTable();

  task_index = index;

  // Set name for table
  QSettings* setTask;
  setTask = new QSettings("ACS", "TASK");

  QStringList tmp;
  tmp << tr("0.0") << tr("0.0") << tr("0.0");

  font.setBold(false);
  font.setPointSize(12);

  for (int i = 0; i < index; i++)
  {

    QTableWidgetItem* item_Name = new QTableWidgetItem(task_code[i]);
    item_Name->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);
    item_Name->setForeground(Qt::black);
    item_Name->setFont(font);

    QTableWidgetItem* item_Z = new QTableWidgetItem(
        setTask->value(task_code[i], tmp).value<QStringList>()[0]);
    item_Z->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);
    item_Z->setForeground(Qt::black);
    item_Z->setFont(font);

    QTableWidgetItem* item_table = new QTableWidgetItem(
        setTask->value(task_code[i], tmp).value<QStringList>()[1]);
    item_table->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);
    item_table->setForeground(Qt::black);
    item_table->setFont(font);

    QTableWidgetItem* item_pusher = new QTableWidgetItem(
        setTask->value(task_code[i], tmp).value<QStringList>()[2]);
    item_pusher->setTextAlignment(Qt::AlignCenter | Qt::AlignVCenter);
    item_pusher->setForeground(Qt::black);
    item_pusher->setFont(font);

    ui->table_task->setItem(i, 0, item_Name);
    ui->table_task->setItem(i, 1, item_Z);
    ui->table_task->setItem(i, 2, item_table);
    ui->table_task->setItem(i, 3, item_pusher);

    // Button save value

    btn_editTask[i] = new QPushButton;
    ui->table_task->setCellWidget(i, 4, btn_editTask[i]);

    btn_editTask[i]->setText("SAVE");
    btn_editTask[i]->setStyleSheet("QPushButton"
                                   "{"
                                   "    background-color: rgb(225, 225, 201);"
                                   "    background-color: #1f425d;"
                                   "    background-color: #3cbaa2;"
                                   "    border: 4px outset #a3a1a1;"
                                   "    font: 75 12pt;"
                                   "    font-weight: 550;"
                                   "    color:rgb(240, 240, 240);"
                                   "}"

                                   "QPushButton:pressed"
                                   "{"
                                   "    border: 4px inset #a3a1a1;"
                                   "    background-color: cyan;"
                                   "    background-color: #3cbaa2;"
                                   "    background-color: rgb(239, 239, 239);"
                                   "    color:rgb(40, 40, 40);"
                                   "    font-weight: 400;"
                                   "}");

    QObject::connect(btn_editTask[i], &QPushButton::clicked,
                     [this, i]() { writeTaskSettings(i); });
  }
}

void set_frame::writeTaskSettings(int index)
{
  QString key_name = task_code[index];

  QStringList value_;
  value_ << ui->table_task->item(index, 1)->text()
         << ui->table_task->item(index, 2)->text()
         << ui->table_task->item(index, 3)->text();

  QSettings* settask;
  settask = new QSettings("ACS", "TASK");

  settask->setValue(key_name, value_);
  set_taskTable(task_index);
}
