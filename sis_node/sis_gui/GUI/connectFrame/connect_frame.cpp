﻿#include "connect_frame.h"
#include "ui_connect_frame.h"

#include <QDebug>

connect_frame::connect_frame(QWidget* parent)
    : QWidget(parent), ui(new Ui::connect_frame)
{
  ui->setupUi(this);
  font.setPointSize(14);
  ft.setBold(false);
  ft.setPointSize(10);
  conn_init_layout();
  conn_init_config();
}

connect_frame::~connect_frame()
{
  for (int i = 0; i < 3; i++)
  {
    delete ledPort_[i];
  }
  delete ui;
}

void connect_frame::closeEvent(QCloseEvent* event)
{
  QWidget::closeEvent(event);
}
void connect_frame::conn_init_layout()
{
  for (int i = 0; i < 3; i++)
  {
    ledPort_[i] = new KLed(this);
    ledPort_[i]->setShape(KLed::Shape::Rectangular);
    ledPort_[i]->setColor(Qt::red);
  }

  for (int i = 0; i < 3; i++)
  {
    ledConv1[i] = new KLed(this);
    ledConv1[i]->setShape(KLed::Shape::Rectangular);
    ledConv1[i]->setColor(Qt::red);
  }

  for (int i = 0; i < 3; i++)
  {
    ledConv2[i] = new KLed(this);
    ledConv2[i]->setShape(KLed::Shape::Rectangular);
    ledConv2[i]->setColor(Qt::red);
  }

  for (int i = 0; i < 2; i++)
  {
    ledEmc[i] = new KLed(this);
    ledEmc[i]->setShape(KLed::Shape::Rectangular);
    ledEmc[i]->setColor(Qt::red);
  }

  for (int i = 0; i < 4; i++)
  {
    ledStp1[i] = new KLed(this);
    ledStp1[i]->setShape(KLed::Shape::Rectangular);
    ledStp1[i]->setColor(Qt::red);
  }

  for (int i = 0; i < 4; i++)
  {
    ledStp2[i] = new KLed(this);
    ledStp2[i]->setShape(KLed::Shape::Rectangular);
    ledStp2[i]->setColor(Qt::red);
  }

  layoutPort[AGV1] = ui->agv1con;
  layoutPort[AGV2] = ui->agv2con;
  layoutPort[LEVEL2] = ui->l2con;

  layoutBat[BAT1] = ui->bat1;
  layoutBat[BAT2] = ui->bat2;

  layoutConv1[IN] = ui->conv1_in;
  layoutConv1[MID] = ui->conv1_mid;
  layoutConv1[ARRIVAL] = ui->conv1_arr;

  layoutConv2[IN] = ui->conv2_in;
  layoutConv2[MID] = ui->conv2_mid;
  layoutConv2[ARRIVAL] = ui->conv2_arr;

  layoutEmc[AGV1] = ui->emc1;
  layoutEmc[AGV2] = ui->emc2;

  layoutStp1[UP1] = ui->up11;
  layoutStp1[DOWN1] = ui->down11;
  layoutStp1[UP2] = ui->up21;
  layoutStp1[DOWN2] = ui->down21;

  layoutStp2[UP1] = ui->up12;
  layoutStp2[DOWN1] = ui->down12;
  layoutStp2[UP2] = ui->up22;
  layoutStp2[DOWN2] = ui->down22;

  layoutBat[AGV1] = ui->bat1;
  layoutBat[AGV2] = ui->bat2;

  pos1[POSX] = ui->line_posX1;
  pos1[POSY] = ui->line_posY1;
  pos1[POSA] = ui->line_posA1;

  pos2[POSX] = ui->line_posX2;
  pos2[POSY] = ui->line_posY2;
  pos2[POSA] = ui->line_posA2;

  posId1 = ui->linePos1;
  posId2 = ui->linePos2;

  btnAuto1[START] = ui->btn1Start;
  btnAuto1[STOP] = ui->btn1Stop;

  btnAuto2[START] = ui->btn2Start;
  btnAuto2[STOP] = ui->btn2Stop;

  btnMan1[MRUN] = ui->btnRun1;
  btnMan1[MSTP] = ui->btnStp1;
  btnConv1[CLOAD] = ui->btnLoad1;
  btnConv1[CUNLOAD] = ui->btnUnload1;
  btnConv1[CSTP] = ui->btnCnvStp1;

  btnMan2[MRUN] = ui->btnRun2;
  btnMan2[MSTP] = ui->btnStp2;
  btnConv2[CLOAD] = ui->btnLoad2;
  btnConv2[CUNLOAD] = ui->btnUnload2;
  btnConv2[CSTP] = ui->btnCnvStp2;

  moveStatus[AGV1] = ui->label_move1;
  moveStatus[AGV2] = ui->label_move2;

  convStatus[AGV1] = ui->label_conv1;
  convStatus[AGV2] = ui->label_conv2;

  posAGV[AGV1] = ui->label_pos1;
  posAGV[AGV2] = ui->label_pos2;
}

void connect_frame::conn_init_config()
{
  for (int i = 0; i < 2; i++)
  {
    connect(btnAuto1[i], &QPushButton::clicked,
            [=]() { btn_auto_clicked(1, i); });
    connect(btnAuto2[i], &QPushButton::clicked,
            [=]() { btn_auto_clicked(2, i); });
    connect(btnMan1[i], &QPushButton::clicked, [=]() { btn_man_clicked(1, i); });
    connect(btnMan2[i], &QPushButton::clicked, [=]() { btn_man_clicked(2, i); });
  }
  for (int i = 0; i < 3; i++)
  {
    connect(btnConv1[i], &QPushButton::clicked,
            [=]() { btn_conv_clicked(1, i); });
    connect(btnConv2[i], &QPushButton::clicked,
            [=]() { btn_conv_clicked(2, i); });
  }
  Vol1 = new QProgressBar();
  Vol1->setMinimum(46);
  Vol1->setMaximum(60);

  Vol1->setVisible(true);
  Vol1->setTextVisible(true);
  Vol1->setFormat(QString("Voltage (V)"));
  Vol1->setAlignment(Qt::AlignCenter);
  int vol1_ = 0;
  // qDebug() << "voltage" << vol_;
  Vol1->setValue(vol1_);
  Vol1->setFormat(QString("%1(V)").arg(vol1_));
  Vol1->setFixedHeight(58);

  layoutBat[AGV1]->addWidget(Vol1);

  Vol2 = new QProgressBar();
  Vol2->setMinimum(46);
  Vol2->setMaximum(60);

  Vol2->setVisible(true);
  Vol2->setTextVisible(true);
  Vol2->setFormat(QString("Voltage (V)"));
  Vol2->setAlignment(Qt::AlignCenter);
  int vol2_ = 0;
  // qDebug() << "voltage" << vol_;
  Vol2->setValue(vol2_);
  Vol2->setFormat(QString("%1(V)").arg(vol2_));
  Vol2->setFixedHeight(58);

  layoutBat[AGV2]->addWidget(Vol2);

  for (int i = 0; i < 2; i++)
    layoutEmc[i]->addWidget(ledEmc[i]);

  for (int i = 0; i < 3; i++)
    layoutPort[i]->addWidget(ledPort_[i]);

  for (int i = 0; i < 4; i++)
  {
    layoutStp1[i]->addWidget(ledStp1[i]);
    layoutStp2[i]->addWidget(ledStp2[i]);
  }

  for (int i = 0; i < 3; i++)
  {
    layoutConv1[i]->addWidget(ledConv1[i]);
    layoutConv2[i]->addWidget(ledConv2[i]);
  }
}

void connect_frame::updateProgress(qint8 id, qint16 voltage)
{
  if (id == 1)
  {
    int vol_ = (int)(voltage / 10);
    if (vol_ > vol_max)
      vol_ = vol_max;
    else if (vol_ < vol_min)
      vol_ = vol_min;
    Vol1->setValue(vol_);

    QPalette p = Vol1->palette();

    if (vol_ <= 49)
      p.setColor(QPalette::Highlight, QColor(Qt::red));
    else if (vol_ < 52)
      p.setColor(QPalette::Highlight, QColor(Qt::yellow));
    else if (vol_ < 56)
      p.setColor(QPalette::Highlight, QColor(Qt::green));
    else
      p.setColor(QPalette::Highlight, QColor(Qt::red));

    Vol1->setPalette(p);

    int remain = vol_ % 10;
    Vol1->setFormat(QString("%1.%2(V)").arg(vol_).arg(remain));
  }
  if (id == 2)
  {
    int vol_ = (int)(voltage / 10);
    if (vol_ > vol_max)
      vol_ = vol_max;
    else if (vol_ < vol_min)
      vol_ = vol_min;
    Vol2->setValue(vol_);

    QPalette p = Vol2->palette();

    if (vol_ <= 49)
      p.setColor(QPalette::Highlight, QColor(Qt::red));
    else if (vol_ < 52)
      p.setColor(QPalette::Highlight, QColor(Qt::yellow));
    else if (vol_ < 56)
      p.setColor(QPalette::Highlight, QColor(Qt::green));
    else
      p.setColor(QPalette::Highlight, QColor(Qt::red));

    Vol2->setPalette(p);

    int remain = vol_ % 10;
    Vol2->setFormat(QString("%1.%2(V)").arg(vol_).arg(remain));
  }
}

void connect_frame::set_btn_Selected(QPushButton* btn, bool isSelect)
{

  if (isSelect)
  {
    btn->setStyleSheet(style_sel);
  }
  else
  {

    btn->setStyleSheet(style_nor);
  }
}

void connect_frame::btn_auto_clicked(int id, int i)
{
  switch (id)
  {
  case 1:
  {
    switch (i)
    {
    case START:
    {
      set_btn_Selected(btnAuto1[START], true);
      set_btn_Selected(btnAuto1[STOP], false);
    }
    break;
    case STOP:
    {
      set_btn_Selected(btnAuto1[START], false);
      set_btn_Selected(btnAuto1[STOP], true);
    }
    break;
    }
  }
  break;
  case 2:
  {
    switch (i)
    {
    case START:
    {
      set_btn_Selected(btnAuto2[START], true);
      set_btn_Selected(btnAuto2[STOP], false);
    }
    break;
    case STOP:
    {
      set_btn_Selected(btnAuto2[START], false);
      set_btn_Selected(btnAuto2[STOP], true);
    }
    break;
    }
  }
  break;
  }
}

void connect_frame::btn_man_clicked(int id, int i)
{
  switch (id)
  {
  case 1:
  {
    switch (i)
    {
    case MRUN:
    {
      set_btn_Selected(btnMan1[MRUN], true);
      set_btn_Selected(btnMan1[MSTP], false);
    }
    break;
    case MSTP:
    {
      set_btn_Selected(btnMan1[MRUN], false);
      set_btn_Selected(btnMan1[MSTP], true);
    }
    break;
    }
  }
  break;
  case 2:
  {
    switch (i)
    {
    case MRUN:
    {
      set_btn_Selected(btnMan2[MRUN], true);
      set_btn_Selected(btnMan2[MSTP], false);
    }
    break;
    case MSTP:
    {
      set_btn_Selected(btnMan2[MRUN], false);
      set_btn_Selected(btnMan2[MSTP], true);
    }
    break;
    }
  }
  break;
  }
}

void connect_frame::btn_conv_clicked(int id, int i)
{
  switch (id)
  {
  case 1:
  {
    switch (i)
    {
    case CLOAD:
    {
      set_btn_Selected(btnConv1[CLOAD], true);
      set_btn_Selected(btnConv1[CUNLOAD], false);
      set_btn_Selected(btnConv1[CSTP], false);
    }
    break;
    case CUNLOAD:
    {
      set_btn_Selected(btnConv1[CLOAD], false);
      set_btn_Selected(btnConv1[CUNLOAD], true);
      set_btn_Selected(btnConv1[CSTP], false);
    }
    break;
    case CSTP:
    {
      set_btn_Selected(btnConv1[CLOAD], false);
      set_btn_Selected(btnConv1[CUNLOAD], false);
      set_btn_Selected(btnConv1[CSTP], true);
    }
    break;
    }
  }
  break;
  case 2:
  {
    switch (i)
    {
    case CLOAD:
    {
      set_btn_Selected(btnConv2[CLOAD], true);
      set_btn_Selected(btnConv2[CUNLOAD], false);
      set_btn_Selected(btnConv2[CSTP], false);
    }
    break;
    case CUNLOAD:
    {
      set_btn_Selected(btnConv2[CLOAD], false);
      set_btn_Selected(btnConv2[CUNLOAD], true);
      set_btn_Selected(btnConv2[CSTP], false);
    }
    break;
    case CSTP:
    {
      set_btn_Selected(btnConv2[CLOAD], false);
      set_btn_Selected(btnConv2[CUNLOAD], false);
      set_btn_Selected(btnConv2[CSTP], true);
    }
    break;
    }
  }
  break;
  }
}

void connect_frame::onLed(KLed* led, bool isOn)
{
  if (isOn)
    led->setColor(Qt::green);
  else if (!isOn)
    led->setColor(Qt::red);
}

void connect_frame::connectState(int id, bool state)
{
  onLed(ledPort_[id], state);
}

void connect_frame::agv_state(qint8 id, qint16 px, qint16 py, qint32 pa,
                              qint8 agv_pos, qint8 move_status,
                              qint8 conv_status, qint16 vol, qint8 data)
{
  if (id == 1)
  {
    pos1[POSX]->setText(QString::number(px / 1000.0, 'f', 2));
    pos1[POSY]->setText(QString::number(py / 1000.0, 'f', 2));
    pos1[POSA]->setText(QString::number(pa / 1000.0, 'f', 2));

    switch (agv_pos)
    {
    case AGV_STP:
      moveStatus[AGV1]->setText("STOP");
      break;
    case AGV_RUN:
      moveStatus[AGV1]->setText("RUN");
      break;
    case OBS_DETECT:
      moveStatus[AGV1]->setText("OBSTACLE");
      break;
    case MANUAL:
      moveStatus[AGV1]->setText("MANUAL");
      break;
    case GOAL_MOVE:
      moveStatus[AGV1]->setText("GOAL");
      break;
    }

    switch (conv_status)
    {
    case CONV_STP:
      convStatus[AGV1]->setText("STOP");
      break;
    case CONV_RUN:
      convStatus[AGV1]->setText("RUN");
      break;
    case MANUAL:
      convStatus[AGV1]->setText("MANUAL");
      break;
    case GOAL_CONV:
      convStatus[AGV1]->setText("GOAL");
      break;
    case CONV_ERR:
      convStatus[AGV1]->setText("ERROR");
      break;
    }

    switch (agv_pos)
    {
    case CHG1:
      posAGV[AGV1]->setText("CHG1");
      break;
    case CHG2:
      posAGV[AGV1]->setText("CHG2");
      break;
    case S_CONV:
      posAGV[AGV1]->setText("SUPPLY CONV");
      break;
    case D_CONV:
      posAGV[AGV1]->setText("DISPOSE CONV");
      break;
    case E_CONV1:
      posAGV[AGV1]->setText("EX CONV #1");
      break;
    case TON_BAG_CONV:
      posAGV[AGV1]->setText("TONBAG CONV");
      break;
    case E_CONV2:
      posAGV[AGV1]->setText("EX CONV #2");
      break;
    case E_CONV3:
      posAGV[AGV1]->setText("EX CONV #3");
      break;
    case IN_CONV:
      posAGV[AGV1]->setText("IN CONV");
      break;
    case OUT_CONV:
      posAGV[AGV1]->setText("OUT CONV");
      break;
    }
    int8_t _data_conv[3];
    int8_t _emc = 0;
    int8_t _data_stp[4];

    _emc = data & 0x01;
    _data_conv[IN] = (data >> 1) & 0x01;
    _data_conv[MID] = (data >> 2) & 0x01;
    _data_conv[ARRIVAL] = (data >> 3) & 0x01;
    _data_stp[UP1] = (data >> 4) & 0x01;
    _data_stp[DOWN1] = (data >> 5) & 0x01;
    _data_stp[UP2] = (data >> 6) & 0x01;
    _data_stp[DOWN2] = (data >> 7) & 0x01;

    if (_emc == 1)
      onLed(ledEmc[AGV1], true);
    else
      onLed(ledEmc[AGV1], false);

    for (int i = 0; i < 3; i++)
    {
      if (_data_conv[i] == 1)
        onLed(ledConv1[i], true);
      else
        onLed(ledConv1[i], false);
    }

    for (int i = 0; i < 4; i++)
    {
      if (_data_stp[i] == 1)
        onLed(ledStp1[i], true);
      else
        onLed(ledStp1[i], false);
    }
  }
  if (id == 2)
  {
    pos2[POSX]->setText(QString::number(px / 1000.0, 'f', 2));
    pos2[POSY]->setText(QString::number(py / 1000.0, 'f', 2));
    pos2[POSA]->setText(QString::number(pa / 1000.0, 'f', 2));

    switch (agv_pos)
    {
    case AGV_STP:
      moveStatus[AGV2]->setText("STOP");
      break;
    case AGV_RUN:
      moveStatus[AGV2]->setText("RUN");
      break;
    case OBS_DETECT:
      moveStatus[AGV2]->setText("OBSTACLE");
      break;
    case MANUAL:
      moveStatus[AGV2]->setText("MANUAL");
      break;
    case GOAL_MOVE:
      moveStatus[AGV2]->setText("GOAL");
      break;
    }

    switch (conv_status)
    {
    case CONV_STP:
      convStatus[AGV2]->setText("STOP");
      break;
    case CONV_RUN:
      convStatus[AGV2]->setText("RUN");
      break;
    case MANUAL:
      convStatus[AGV2]->setText("MANUAL");
      break;
    case GOAL_CONV:
      convStatus[AGV2]->setText("GOAL");
      break;
    case CONV_ERR:
      convStatus[AGV2]->setText("ERROR");
      break;
    }

    switch (agv_pos)
    {
    case CHG1:
      posAGV[AGV2]->setText("CHG1");
      break;
    case CHG2:
      posAGV[AGV2]->setText("CHG2");
      break;
    case S_CONV:
      posAGV[AGV2]->setText("SUPPLY CONV");
      break;
    case D_CONV:
      posAGV[AGV2]->setText("DISPOSE CONV");
      break;
    case E_CONV1:
      posAGV[AGV2]->setText("EX CONV #1");
      break;
    case TON_BAG_CONV:
      posAGV[AGV2]->setText("TONBAG CONV");
      break;
    case E_CONV2:
      posAGV[AGV2]->setText("EX CONV #2");
      break;
    case E_CONV3:
      posAGV[AGV2]->setText("EX CONV #3");
      break;
    case IN_CONV:
      posAGV[AGV2]->setText("IN CONV");
      break;
    case OUT_CONV:
      posAGV[AGV2]->setText("OUT CONV");
      break;
    }
    int8_t _data_conv[3];
    int8_t _emc = 0;
    int8_t _data_stp[4];

    _emc = data & 0x01;
    _data_conv[IN] = (data >> 1) & 0x01;
    _data_conv[MID] = (data >> 2) & 0x01;
    _data_conv[ARRIVAL] = (data >> 3) & 0x01;
    _data_stp[UP1] = (data >> 4) & 0x01;
    _data_stp[DOWN1] = (data >> 5) & 0x01;
    _data_stp[UP2] = (data >> 6) & 0x01;
    _data_stp[DOWN2] = (data >> 7) & 0x01;

    if (_emc == 1)
      onLed(ledEmc[AGV2], true);
    else
      onLed(ledEmc[AGV2], false);

    for (int i = 0; i < 3; i++)
    {
      if (_data_conv[i] == 1)
        onLed(ledConv2[i], true);
      else
        onLed(ledConv2[i], false);
    }

    for (int i = 0; i < 4; i++)
    {
      if (_data_stp[i] == 1)
        onLed(ledStp2[i], true);
      else
        onLed(ledStp2[i], false);
    }
  }
}
