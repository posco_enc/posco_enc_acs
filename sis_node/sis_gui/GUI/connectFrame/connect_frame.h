#ifndef CONNECT_FRAME_H
#define CONNECT_FRAME_H

#include <GUI/object/kled.h>
#include <QPushButton>
#include <QWidget>

#include <QGridLayout>
#include <QLineEdit>
#include <QLabel>
#include <QTableWidget>
#include <QProgressBar>

#include <GUI/config/btn_style.h>
#include <string>

#define AGV1 0
#define AGV2 1
#define LEVEL2 2

#define BAT1 0
#define BAT2 1

#define IN 0
#define MID 1
#define ARRIVAL 2

#define UP1 0
#define DOWN1 1
#define UP2 2
#define DOWN2 3

#define POSX 0
#define POSY 1
#define POSA 2

#define START 0
#define STOP 1

#define MRUN 0
#define MSTP 1
#define CLOAD 0
#define CUNLOAD 1
#define CSTP 2

#define CHG1 0
#define CHG2 1
#define S_CONV 2
#define D_CONV 3
#define TON_BAG_CONV 4
#define E_CONV1 5
#define E_CONV2 6
#define E_CONV3 7
#define IN_CONV 8
#define OUT_CONV 9

#define AGV_STP 0
#define AGV_RUN 1
#define GOAL_MOVE 3
#define OBS_DETECT 2
#define MANUAL 4

#define CONV_STP 0
#define CONV_RUN 1
#define GOAL_CONV 3
#define CONV_ERR 2




namespace Ui
{
class connect_frame;
}

class QLabel;
class KLed;


class connect_frame : public QWidget
{
  Q_OBJECT

public:
  explicit connect_frame(QWidget* parent = 0);
  ~connect_frame();

  void closeEvent(QCloseEvent* event);

  void conn_init_layout();
  void conn_init_config();


public slots:
  void agv_state(qint8, qint16, qint16, qint32, qint8, qint8, qint8, qint16, qint8);
  void connectState(int, bool);
  void updateProgress(qint8, qint16);

private slots:

  void btn_auto_clicked(int id, int i);
  void btn_man_clicked(int id, int i);
  void btn_conv_clicked(int id, int i);


signals:
  void CMD_task(quint8 id_, quint8 cmd_, QString pos_, qint8 conv);
  void auto_cmd(qint8, qint8);

private:
  Ui::connect_frame* ui;

  QFont font, ft;

  QGridLayout* layoutPort[3];
  KLed* ledPort_[3];

  QGridLayout* layoutBat[2];

  QGridLayout* layoutConv1[3];
  KLed* ledConv1[3];

  QGridLayout* layoutConv2[3];
  KLed* ledConv2[3];

  QGridLayout* layoutEmc[2];
  KLed* ledEmc[2];

  QGridLayout* layoutStp1[4];
  KLed* ledStp1[4];

  QGridLayout* layoutStp2[4];
  KLed* ledStp2[4];

  QLineEdit* pos1[3];
  QLineEdit* pos2[3];
  QLabel* moveStatus[2];
  QLabel* convStatus[2];
  QLabel* posAGV[2];

  bool isConnect[3];
  void onLed(KLed* led, bool isOn);

  QPushButton* btnAuto1[2];
  QPushButton* btnAuto2[2];
  QPushButton* btnMan1[2];
  QPushButton* btnMan2[2];
  QPushButton* btnConv1[2];
  QPushButton* btnConv2[2];

  QLineEdit* posId1;
  QLineEdit* posId2;

  void set_btn_Selected(QPushButton* btn, bool isSelect);

  QProgressBar *Vol1;
  QProgressBar *Vol2;
  int16_t vol_min = 46;
  int16_t vol_max = 62;
  QString task_pos = "";

};

#endif // CONNECT_FRAME_H
