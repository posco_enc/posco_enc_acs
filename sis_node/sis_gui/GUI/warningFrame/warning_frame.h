#ifndef WARNING_FRAME_H
#define WARNING_FRAME_H

#include <QWidget>
#include <QComboBox>
#include <QDate>

namespace Ui {
class warningFrame;
}

class warningFrame : public QWidget
{
    Q_OBJECT

public:
    explicit warningFrame(QWidget *parent = nullptr);
    ~warningFrame();

    void init_layout();
    void init_table();
    void init_config();


private:
    Ui::warningFrame *ui;
    QFont font;
    int8_t ct[2];
    
    QComboBox *cb_Year;
    QComboBox *cb_Month;
    QComboBox *cb_Date;
};

#endif // WARNING_FRAME_H
