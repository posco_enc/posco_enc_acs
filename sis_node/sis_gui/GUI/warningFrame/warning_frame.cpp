#include "warning_frame.h"
#include "ui_warning_frame.h"
#include <QTableWidgetItem>

#include <QDebug>

warningFrame::warningFrame(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::warningFrame)
{
    ui->setupUi(this);

    init_layout();
    init_table();
    init_config();
}

warningFrame::~warningFrame()
{
    delete ui;
}

void warningFrame::init_layout()
{
    cb_Year = ui->comboBox;
    cb_Month = ui->comboBox_2;
    cb_Date = ui->comboBox_3;
    
    QDate date = QDate::currentDate();

    cb_Year->setItemText( 0, QString::number(date.year()));
    cb_Month->setItemText(0, QString::number(date.month()));
    cb_Date->setItemText( 0, QString::number(date.day()));
}

void warningFrame::init_table()
{

}

void warningFrame::init_config()
{
    font.setPointSize(15);
    for(int i = 0;i++;i<2)
    {
        ct[i] = 0;
    }
}
