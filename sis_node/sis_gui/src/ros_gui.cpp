
#include <stdio.h>
#include <ros/ros.h>

#include <QApplication>
#include <QString>
#include <QByteArray>

#include "GUI/mainwindow.h"
#include "TCP/client.h"
#include "TCP/server.h"
#include "Vizualization/myviz.h"

#include <QUrl>
//#include <QWebEngineView>
//#include <QWebEngineSettings>

int main(int argc, char **argv)
{

    ros::init(argc, argv, "sis_ros_gui");
    ros::NodeHandle nh;

    QApplication app(argc, argv);

    //GUI
    MainWindow window;
    window.show();

	//TCP
	Server *server = new Server(49153,&window); // nullptr or & 
	Client *client = new Client(QString("192.168.2.3"), 49154, &window);
//	client->doConnect();

//	MyViz *viz = new MyViz();
//	viz->show();
	//connect
	QObject::connect( &window, SIGNAL(doConnect(QString, quint16)), client, SLOT(doConnect(QString, quint16)) );
	QObject::connect( &window, SIGNAL(doDisconnect()), client, SLOT(doDisConnect()) );
	QObject::connect( &window, SIGNAL(doSend(QString)), client, SLOT(doSend(QString)) );

	QObject::connect( server, SIGNAL(readData(QByteArray)), &window, SLOT(readData(QByteArray)) );
	QObject::connect( server, SIGNAL(readData(QString)), &window, SLOT(readData(QString)) );

	QObject::connect( client, SIGNAL(readData(QByteArray)), &window, SLOT(readData(QByteArray)) );
	QObject::connect( client, SIGNAL(readData(QString)), &window, SLOT(readData(QString)) );
/*
	//toDo 
    QWebEngineView *webview = new QWebEngineView;
    QUrl url = QUrl("https://www.naver.com/");

    webview->page()->load(url);
    QObject::connect(webview->page(), &QWebEnginePage::loadFinished, this, on_PageLoadFinished);
//    ui->verticalLayout->addWidget(webview);
    QWebEngineSettings::globalSettings()->setAttribute(QWebEngineSettings::PluginsEnabled, true);
    QWebEngineSettings::defaultSettings()->setAttribute(QWebEngineSettings::PluginsEnabled, true);
    webview->show();
*/
	////////////////////////////////////////////////////
	app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));
	return app.exec();
}

