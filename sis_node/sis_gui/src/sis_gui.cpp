// Modify 200706
//#include "GUI/mainFrame/mainwindow.h"
#include "GUI/mainGUI/maingui.h"
#include <QApplication>
#include <ros/ros.h>

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "sis_ros_gui");
    QApplication app(argc, argv);

//    AGV_Command AGV1,AGV2;

    // Modify 200706
    MainGUI w;
 //   MainWindow w;
//    w.showMaximized();
    w.showFullScreen();

	app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));
    return app.exec();
}
