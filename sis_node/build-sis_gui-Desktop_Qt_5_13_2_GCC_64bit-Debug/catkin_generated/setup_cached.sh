#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/sarang/acs_ws/src/sis_node/build-sis_gui-Desktop_Qt_5_13_2_GCC_64bit-Debug/devel:/opt/Qt5.13.2/5.13.2/gcc_64"
export LD_LIBRARY_PATH="/opt/Qt5.13.2/5.13.2/gcc_64/lib"
export PATH="/opt/Qt5.13.2/5.13.2/gcc_64/bin:/opt/Qt5.13.2/5.13.2/gcc_64/bin/:/opt/Qt5.13.2/Tools/QtCreator/bin/:/home/sarang/bin:/home/sarang/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin"
export PKG_CONFIG_PATH="/opt/Qt5.13.2/5.13.2/gcc_64/lib/pkgconfig"
export PWD="/home/sarang/acs_ws/src/sis_node/build-sis_gui-Desktop_Qt_5_13_2_GCC_64bit-Debug"
export PYTHONPATH=""